# README #

This repository grant access to the rasperry pi radar extension board RAPID. 

### What is this repository for? ###

* Version 0.1.1

### How do I get set up? ###

* from scratch

```
#!bash

git clone https://bitbucket.org/radarpi/rapidpatch.git
cd rapidpatch
sh install_patch.sh
```

* from existing rapidpatch

```
#!bash
cd ${PATH_TO_RAPIDPATCH}$
git pull
sh install_patch.sh

```